window.addEventListener("load", () => {
    //Створюємо масив із кнопок для майбутньої обробки елементів із масиву.
    const [...btns] = document.querySelectorAll("button");
    //Викликаємо подію при натиску на клавішу, за допомогою функції клір очищаємо стилі клавіш, присвоюємо змінну натиснутій клавіші за допомогою ід та кіКод, добавляємо до вибраної клавіші стиль.
    window.addEventListener("keydown", (e) => {
        clear();
        let key = document.getElementById(`${e.keyCode}`);
        getColor(key);
    });

    function getColor(el) {
        el.style.backgroundColor = "blue";
        el.style.transform = "scale(1.4)";
    };

    function clear() {
        btns.forEach((e) => {
            e.style.backgroundColor = "#33333a";
            e.style.transform = "scale(1)";
        });
    };
});
const header = document.createElement("h2");
header.textContent = "Task №1 Timer";
document.body.append(header);


let rightPos = 0;
const animateFirst = () => {
  header.style.left = rightPos + "px";
  rightPos += 1;
  if (rightPos > 900) {
    rightPos = 100
  }
}
setInterval(animateFirst, 10);

const div = document.createElement("div");
div.classList.add("timer");
document.body.append(div);

const timerScreen = document.createElement("div");
timerScreen.classList.add("timer_screen");
timerScreen.classList.add("black");
timerScreen.id = "timer_screen";
timerScreen.innerHTML = `<span>00</span>:<span>00</span>:<span>00</span>`;
timerScreen.children[0].classList = "timer_hours";
timerScreen.children[1].classList = "timer_minutes";
timerScreen.children[2].classList = "timer_seconds";
div.append(timerScreen);

const btns = document.createElement("div");
btns.classList.add("button_list");
div.append(btns);

const startButton = document.createElement("button");
startButton.id = "start_btn";
startButton.classList = "button";
startButton.value = "START";
startButton.innerText = "START";
btns.append(startButton);

const stopButton = document.createElement("button");
stopButton.id = "stop_btn";
stopButton.classList = "button";
stopButton.value = "stop";
stopButton.innerText = "STOP";
btns.append(stopButton);

const resetButton = document.createElement("button");
resetButton.id = "reset_btn";
resetButton.classList = "button";
resetButton.value = "reset";
resetButton.innerText = "RESET";
btns.append(resetButton);

const get = (id) => document.getElementById(id);

let hours = 0,
  minutes = 0,
  seconds = 0,
  intervalHandler;

firstZeroSeconds = () => {
  if (seconds < 10) {
    document.querySelector(".timer_seconds").textContent = `0${seconds}`;
  } else {
    document.querySelector(".timer_seconds").textContent = seconds;
  }
};

firstZeroMinutes = () => {
  if (minutes < 10) {
    document.querySelector(".timer_minutes").textContent = `0${minutes}`;
  } else {
    document.querySelector(".timer_minutes").textContent = minutes;
  }
};

firstZeroHours = () => {
  if (hours < 10) {
    document.querySelector(".timer_hours").textContent = `0${hours}`;
  } else {
    document.querySelector(".timer_hours").textContent = hours;
  }
};

const count = () => {
  seconds++;
  firstZeroSeconds();
  if (seconds > 59) {
    seconds = 0;
    firstZeroSeconds();
    minutes++;
    firstZeroMinutes();
  }

  if (minutes > 59) {
    minutes = 0;
    firstZeroMinutes();
    hours++;
    firstZeroHours();
  }

  if (hours > 24) {
    hours = 0;
    firstZeroHours();
  }
};

get("start_btn").onclick = () => {
  intervalHandler = setInterval(count, 1000);
  timerScreen.classList.add("green");
  timerScreen.classList.remove("black");
  timerScreen.classList.remove("silver");
};

get("stop_btn").onclick = () => {
  clearInterval(intervalHandler);
  timerScreen.classList.add("red");
  timerScreen.classList.remove("green");
};

get("reset_btn").onclick = () => {
  clearInterval(intervalHandler);
    hours = 0,
    minutes = 0,
    seconds = 0,
    timerScreen.innerHTML = `<span>00</span>:<span>00</span>:<span>00</span>`;
  timerScreen.classList.add("silver");
  timerScreen.classList.remove("green");
  timerScreen.classList.remove("red");
};






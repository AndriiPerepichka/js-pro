const header3 = document.createElement("h2");
header3.textContent = "Task №3 Slider";
header3.id = "header_task3";
document.body.append(header3);

let rightPosit = 0;
const animateThird = () => {
    header3.style.left = rightPosit + "px";
    rightPosit += 1;
    if (rightPosit > 900) {
        rightPosit = 100
    }
}
setInterval(animateThird, 10);

const divContainer = document.createElement("div");
divContainer.id = "container";
document.body.append(divContainer);

const carsArray = [
    "url(https://v5.ua.news/wp-content/uploads/2022/06/tesla-model-s-2021-plaid-exterior-hevcars-3.jpeg?528978)",
    "url(http://avatars.mds.yandex.net/get-verba/787013/2a0000018064f206d8a6e1208f67d4a124b8/cattouch)",
    "url(https://i.infocar.ua/i/2/5590/104021/1920x.jpg)",
    "url(https://img-c.drive.ru/models.photos/0000/000/000/001/966/48d87b714a8833e3-large.jpg)",
    "url(https://i.infocar.ua/i/12/4757/1400x936.jpg)"
];

setInterval(() => {
    divContainer.style.backgroundImage = carsArray[Math.floor(Math.random() * 5)];
}, 3000);
const doc = {
    title: "",
    body: "",
    footer: "",
    date: "",
    app: {
        title: "",
        body: "",
        footer: "",
        date: ""
    },
    showTitle: function (userTitle) {
        doc.title = userTitle;
        document.getElementById("title").innerHTML = `Ваш заголовок: ${userTitle}`;
    },
    showBody: function (userBody) {
        doc.body = userBody;
        document.getElementById("body").innerHTML = `Ваш текст: ${userBody}`;
    },
    showFooter: function (userFooter) {
        doc.footer = userFooter;
        document.getElementById("footer").innerHTML = `Ваш "підвал": ${userFooter}`;
    },
    showDate: function (userDate) {
        doc.date = userDate;
        document.getElementById("date").innerHTML = `Дата заповнення: ${userDate}`;
    },
    showAppTitle: function (userAppTitle) {
        doc.app.title = userAppTitle;
        document.getElementById("app_title").innerHTML = `Заголовок додатка: ${userAppTitle}`;
    },
    showAppBody: function (userAppBody) {
        doc.body = userAppBody;
        document.getElementById("app_body").innerHTML = `Ваш текст: ${userAppBody}`;
    },
    showAppFooter: function (userAppFooter) {
        doc.footer = userAppFooter;
        document.getElementById("app_footer").innerHTML = `Ваш "підвал": ${userAppFooter}`;
    },
    showAppDate: function (userAppDate) {
        doc.date = userAppDate;
        document.getElementById("app_date").innerHTML = `Дата заповнення: ${userAppDate}`;
    },
};

doc.showTitle(prompt("Введіть назву заголовка:"));
doc.showBody(prompt("Введіть основний текст:"));
doc.showFooter(prompt("Заповніть 'підвал' сайту:"));
doc.showDate(prompt("Дата заповнення:"));
doc.showAppTitle(prompt("Введіть заголовок додатку:"));
doc.showAppBody(prompt("Введіть основний текст додатку:"));
doc.showAppFooter(prompt("Заповніть 'підвал' додатку:"));
doc.showAppDate(prompt("Дата заповнення додатку:"));


//--------------------------- Class work ------------------------------
//..............Task 2..................
const obj = {
    "Коля": 1000, 
    "Вася":500, 
    "Петя": 200,
};
document.getElementById("salary").innerHTML = `Зарплата Колі = ${obj.Коля} <br> Зарплата Петі = ${obj.Петя}`;

//.............Task 8...................
const objNew = {};
function isEmpty(object) {
    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};

console.log(isEmpty(objNew));
